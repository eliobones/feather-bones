# Installing feather-bones

- [Prerequisites](/eliobones/feather-bones/prerequisites.html)

## npm

Install into your SASS projects.

```
npm install @elioway/feather-bones
yarn add @elioway/feather-bones
```
