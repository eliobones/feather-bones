# Quickstart feathers-bones

- [feathers-bones Prerequisites](/eliobones/feather-bones/prerequisites.html)
- [Installing feathers-bones](/eliobones/feather-bones/installing.html)

## Nutshell

- Sort your artwork

## Creating users

```
curl -X POST 'http://localhost:3030/person/' \
  -H 'Content-Type: application/json' \
  --data-binary '{ "name": "Super Admin", "email": "god@elioway.com", "password": "letmein" }'

curl -X POST 'http://localhost:3030/person/' \
  -H 'Content-Type: application/json' \
  --data-binary '{ "name": "Gabrielle", "email": "angel1@elioway.com", "password": "letmein" }'

curl -X POST 'http://localhost:3030/person/' \
  -H 'Content-Type: application/json' \
  --data-binary '{ "name": "Micheal", "email": "angel2@elioway.com", "password": "letmein" }'
```

## Authenticate

```
curl 'http://localhost:3030/person/' \
  -H "Authorization: Bearer TOKEN"
```

## Creating Things

```
curl -X POST 'http://localhost:3030/thing/ZpWxFpsxZlv9kFTJ' \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer TOKEN" \
  --data-binary '{ "name": "Blue Thing", "description": "I am a blue thing." }'

curl -X PATCH 'http://localhost:3030/thing/ZpWxFpsxZlv9kFTJ' \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer TOKEN" \
  --data-binary '{ "name": "Blue Thing", "description": "I am a blue thing.", "alternateName": "The sky's the limit!" }'

curl -X DEL 'http://localhost:3030/thing/ZpWxFpsxZlv9kFTJ' \
  -H "Authorization: Bearer TOKEN"

curl -X POST 'http://localhost:3030/thing/' \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer TOKEN" \
  --data-binary '[{ "name": "Blue Thing", "description": "I am a blue thing." }, { "name": "Red Thing", "description": "I am a red thing." }, ]'
```
