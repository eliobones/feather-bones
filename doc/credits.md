# feather-bones Credits

## Artwork

- [art](https://commons.wikimedia.org/)

## Core Thanks!

- [feathersjs](https://feathersjs.com)
- [feathers-mongoose](https://github.com/feathersjs-ecosystem/feathers-mongoose)
