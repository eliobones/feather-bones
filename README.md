![](https://elioway.gitlab.io/eliobones/elio-feather-bones-logo.png)

# feather-bones

> but feathered soon and fledge They summed their pens; and, soaring the air sublime, **the elioWay**

## About

This project uses [FeathersJs](http://feathersjs.com) to deliver a REST API built on <https://schema.org> Types, **the elioWay**.

- [feathers-bones Documentation](https://elioway.gitlab.io/eliobones/feathers-bones/)

## Installing

- [Installing feathers-bones](https://elioway.gitlab.io/eliobones/feathers-bones/installing.html)

## Requirements

- [eliobones Prerequisites](https://elioway.gitlab.io/eliobones/installing.html)

## Getting Started

Getting up and running is as easy as 1, 2, 3.

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Install your dependencies

```
cd path/to/feather-bones
npm install
```

1. Start your app

```
npm start
```

1. [feathers-bones Quickstart](https://elioway.gitlab.io/eliobones/feathers-bones/quickstart.html)

## Testing

Simply run `npm test` and all your tests in the `test/` directory will be run.

## Scaffolding

Feathers has a powerful command line interface. Here are a few things it can do:

```
$ npm install -g @feathersjs/cli          # Install Feathers CLI

$ feathers generate service               # Generate a new Service
$ feathers generate hook                  # Generate a new Hook
$ feathers help                           # Show all commands
```

## Help

For more information on all the things you can do with Feathers visit [docs.feathersjs.com](http://docs.feathersjs.com).

![](https://elioway.gitlab.io/elioolympians/feathers-bones/apple-touch-icon.png)

## License

[MIT](LICENSE) [Tim Bushell](mailto:theElioWay@gmail.com)
