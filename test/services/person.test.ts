import assert from "assert"
import app from "../../bones/app"

describe("'Person' service", () => {
  it("registered the service", () => {
    const service = app.service("person")

    assert.ok(service, "Registered the service")
  })
})
