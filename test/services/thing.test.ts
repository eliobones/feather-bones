import assert from "assert"
import app from "../../bones/app"

describe("'thing' service", () => {
  it("registered the service", () => {
    const service = app.service("thing")

    assert.ok(service, "Registered the service")
  })
})
