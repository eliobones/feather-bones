import { Application } from "../../declarations"
import { ServiceAddons } from "@feathersjs/feathers"
import { Thing } from "./thing.class"
import createModel from "../../models/thing.model"
import hooks from "./thing.hooks"

declare module "../../declarations" {
  interface ServiceTypes {
    thing: Thing & ServiceAddons<any>
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get("paginate"),
  }

  app.use("/thing", new Thing(options, app))

  const service = app.service("thing")

  service.hooks(hooks)
}
