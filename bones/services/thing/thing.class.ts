import { Application } from "../../declarations"
import { Params } from "@feathersjs/feathers"
import { Service, NedbServiceOptions } from "feathers-nedb"

export interface ThingData {
  _id?: string
  name: string
  additionalType?: string
  alternateName?: string
  description?: string
  disambiguatingDescription?: string
  identifier?: string
  image?: string
  mainEntityOfPage?: string
  potentialAction?: string
  sameAs?: string
  subjectOf?: string
  url?: string
}

export class Thing extends Service {
  //eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(options: Partial<NedbServiceOptions>, app: Application) {
    super(options)
  }

  create(data: ThingData, params?: Params) {
    return super.create(data, params)
  }
}
