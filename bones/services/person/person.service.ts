import { Application } from "../../declarations"
import { Person } from "./person.class"
import { ServiceAddons } from "@feathersjs/feathers"
import createModel from "../../models/person.model"
import hooks from "./person.hooks"

declare module "../../declarations" {
  interface ServiceTypes {
    person: Person & ServiceAddons<any>
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get("paginate"),
  }

  app.use("/person", new Person(options, app))

  const service = app.service("person")

  service.hooks(hooks)
}
