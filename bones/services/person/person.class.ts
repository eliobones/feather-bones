import { Application } from "../../declarations"
import { Params } from "@feathersjs/feathers"
import { Service, NedbServiceOptions } from "feathers-nedb"
import { ThingData } from "../thing/thing.class"

interface PersonData extends ThingData {
  email: string
  password: string
  familyName?: string
  givenName?: string
  birthDate?: Date
}

export class Person extends Service {
  //eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(options: Partial<NedbServiceOptions>, app: Application) {
    super(options)
  }

  create(data: PersonData, params?: Params) {
    return super.create(data, params)
  }
}
