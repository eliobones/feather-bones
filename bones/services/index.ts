import { Application } from "../declarations"
import person from "./person/person.service"

import thing from "./thing/thing.service"

export default function (app: Application): void {
  app.configure(person)
  app.configure(thing)
}
